@extends("layout.layout")

@section('content')
<!doctype html>
<html lang="fr" xmlns:width="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voir un Sav</title>
</head>
<body>

<div class="row card p-3 m-2">


    <form method="POST" action="reparer.store" >
        @csrf
        <div class="p-2">
            <div class="">
                <label class="control-label">
                    Designation

                </label>
                <input name="Designation" disabled value="{{$reparer->Designation}}" type="text" class="form-control">

            </div>

            <div class="">
                <label class="control-label" for="" >
                    Numero de serie

                </label>
                <input name="NumSerie" disabled value="{{$reparer->numserie}}" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Date Reparation
                </label>
                <input name="date" value="{{$reparer->date}}" type="text" disabled class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Methode de Réparation

                </label>
                <textarea name="Methode" disabled type="text"  class="form-control">{{$reparer->Motif}}</textarea>
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Methode de Réparation

                </label>
                <textarea name="Methode" disabled type="text"  class="form-control">{{$reparer->Methode}}</textarea>
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Nom client
                </label>
                <input name="Nomclient" disabled value="{{$reparer->Nomclient}}" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Contact Client

                </label>
                <input name="ContactClient" disabled value="{{$reparer->ContactClient}}" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Nom Technicien

                </label>
                <input name="NomTech"  disabled value="{{$reparer->NomTech}}" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Contact Technicien

                </label>
                <input  name="ContactTech" disabled value="{{$reparer->ContactTech}}" type="tel" class="form-control ">
            </div>


    </form>
</div>
</div>
</body>
</html>
@stop

