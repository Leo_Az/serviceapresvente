@component('mail::message')

# Valide ton compte!

Salut {{$user->name}},

clique sur le bouton ci-dessous pour valider ton compte!

@component('mail::button', ['url' => route('confirmation.store',[$user,$user->token])])
valider mon compte
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
