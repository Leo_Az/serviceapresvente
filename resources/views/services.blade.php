<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<title>Nos Services</title>
			<link rel="stylesheet" href="Styles/services.css">
			<link rel="stylesheet" type="text/css" href="Styles/CodeCSS.css">
			<link rel="stylesheet" href="Styles/responsivite.css"> 
			<script type="text/javascript" src="Styles/jQuery.js"></script>
		</head>
		<body>
			

			<header class="headerservice">
				
				<div class="contMenu" id="contMenu">
					<div class="logo">
						<img class="image" src="Images/mt-logo.png">
						<p class="Mediatec">MediaTec SARL</p>
					</div>

					<!--   Le Menu et L entete du SAV  -->
					<div class="menu">
						<ul>
							<li class=""><a href="/">ACCUEIL</a></li>
							<li class="activemenu"><a href="services">NOS SERVICES</a></li>
							<li><a href="contacter">CONTACTER</a></li>
						</ul>
					</div>
					<div class="SAVconnecte">
						<a href="inscription/create"><button class="sInscrire">S'INSCRIRE AU SAV</button></a>
						<a href="login"><button class="seConnecter">CONNEXION</button></a>
					</div>
				</div>
				
			<section class="sectionService">
				<aside class="asideservice">
					<h1>Les Services MediaeTeC</h1>
					<section id="pagi1" class="listeservice1">
						<div class="image1"><p>Développement Web</p></div>
						<div class="image2"><p>Maintenance</p></div>
						<div class="image3"><p>Vente</p></div>
					</section>
					<section id="pagi2" class="listeservice1 pagi2">
						<div class="image4"><p>Maketting</p></div>
						<div class="image5"><p>Cablage</p></div>
						<div class="image6"><p>Installation</p></div>
					</section>
					<button class="btn1 active">1</button>
					<button class="btn2">2</button>
				</aside>
			</section>

			</header>

			
			<!--   Le Footer ou pied de page du SAV  -->
			<footer class="footerservice">
				<div class="dive1">
					<div class="logof">
						<img src="Images/mth-logo.png">
						<p class="Mediate">MediaTec SARL</p>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium amet accusamus iure illum quidem saepe nam veritatis porro, ducimus aperiam delectus. <br><br> Architecto quos similique sit libero officia molestias accusantium ipsa a facilis sunt ab maxime dolorum modi labore? 
						<br><br>
						<b class="CopyRight"style=" font-size: 12px; color: #666">CopyRight 2019 MediaTeC || Solution</b>
					</p>
				</div>


				<div class="dive2">
					<p class="service"><a href="services.html">NOS SERVICES</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					<p style="position: relative; top: -100px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo nihil assumenda architecto, officia totam facere.</p>
				</div>


				<div class="dive3">
					<p class="service"><a href="contacter.html">CONTACTS</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					
				</div>



				<div class="dive4">
					<p class="service"><a href="#">PARTENAIRES</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					<div class="partner">
						<div><a href=""><img style="height: 50px; width: 50px; top: 0px;" src="Images/HP_logo_2012.png" alt=""></a></div>
						<div><a href=""><img style="height: 45px; width: 55px; top: 0px;" src="Images/Dell_Logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 15px;" src="Images/Canon_logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/Lexmark-Logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/microsoft_PNG16.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/Cisco_logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 60px;" src="Images/D-Link.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 60px;" src="Images/Lenovo-logo-2015.png" alt=""></a></div>
					</div>
				</div>	
			</footer>
		</body>
	</html>
	<script type="text/javascript" src="Styles/service.js"></script>
	<script type="text/javascript">
		$(document).on('click', '.asideservice button', function(){
			$(this).addClass('active').siblings().removeClass('active');
		});
		$(document).on('click', '.btn2', function(){
			$('#pagi1').addClass('pagi1');
			$('#pagi2').removeClass('pagi2');
		});
		$(document).on('click', '.btn1', function(){
			$('#pagi2').addClass('pagi2');
			$('#pagi1').removeClass('pagi1');
		});
	</script>