<!DOCTYPE html>
	<html lang="fr">
		<head>
			<meta charset="utf-8">
			<title>Contacter</title>
			<link rel="stylesheet" href="Styles/contacter.css">
			<link rel="stylesheet" type="text/css" href="Styles/CodeCSS.css">
			<link rel="stylesheet" href="Styles/responsivite.css">

			<script type="text/javascript" src="Styles/jQuery.js"></script>
		</head>
		<body>

			<header class="headercontact">
				
				<div class="contMenu" id="contMenu">
					<div class="logo">
						<img class="image" src="Images/mt-logo.png">
						<p class="Mediatec">MediaTec SARL</p>
					</div>

					<!--   Le Menu et L entete du SAV  -->
					<div class="menu">
						<ul>
							<li class=""><a href="/">ACCUEIL</a></li>
							<li><a href="services">NOS SERVICES</a></li>
							<li class="activemenu"><a href="contacter">CONTACTER</a></li>
						</ul>
					</div>
					<div class="SAVconnecte">
						<a href="inscription/create"><button class="sInscrire">S'INSCRIRE AU SAV</button></a>
						<a href="login"><button class="seConnecter">CONNEXION</button></a>
					</div>
				</div>
			</header>

			<section class="SectionContact">
				
				<aside class="acsideContact">
					<h1>Laissez un Message</h1>
					<p>
						Vous avez une question sur nos Services: remplissez ce formulaire. Nous vous apporterons une réponse rapide. 
					</p>
					@if($errors->any())


						<div class="alert alert-warning alert-dismissible fade show" role="alert">
							@foreach($errors->all() as $error)


								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<li>
									{!!$error!!}
								</li>
							@endforeach
						</div>
					@endif
					<form method="post" action="/contacter">
						@csrf

						<input type="text" name="name"  placeholder=" Nom ...">

						<input type="email" name="email"  placeholder=" E-mail ..." style=" margin-left: 25px;">

						<input type="text" name="objet"  placeholder=" Objet ..." style=" margin-left: 25px;"><br>

						<textarea  name="Message" placeholder="  Message ..."  style=" margin-bottom: 10px; margin-top: 30px;"></textarea><br>

						<input type="submit" value=" Envoyer ">
					</form>

				</aside>
				
				<hr style="top: 580px; color: #eee; margin-right: 70px;" align="center">
				<h1 style="position: absolute; top: 600px; left: 50%; transform: translate(-50%, 0); color: #fff; font-size: 36px">Nos Contacts</h1>
				<div class="contacts">
					<table>
						<tr >
							<td style="padding: 0 100px; width: 50%">
								<img src="Images/mobile-phone-logo-png-1.png" style="height: 50px;width: 50px;"><i style=" position: absolute; margin-top: 30px">: ( +225 ) 20 21 88 44</i>
							</td>  
							<td style="padding: 0 100px; width: 50%">
								<img src="Images/fax 2.png" style="height: 50px;width: 50px;"><i style=" position: absolute; margin-top: 30px; margin-left: 20px">: ( +225 ) 20 22 14 57</i>
							</td>
						</tr>
						<tr>
							<td style="padding: 0 100px; width: 50%">
								<img src="Images/icons enveloppe.png" style="height: 50px;width: 50px;"><i style=" position: absolute; margin-top: 30px">: infos@mediatec-ci.com</i>
							</td>  
							<td style="padding: 0 100px; width: 50%">
								<img src="Images/globe-www-512.png" style="height: 65px;width: 65px;"><i style=" position: absolute; margin-top: 36px">: www.mediatec-ci.com</i>
							</td>
						</tr>
					</table>
					<span>
						<img src="Images/icons map.png" style="height: 70px; width: 70px;"><i style=" position: absolute; margin-top: 50px; margin-left: 20px">: Abidjan / Plateau-Pyramide face à la Station TOTAL après MUGEFCI, Entre Orange et AMSA.</i>
					</span>
				</div>
				
			</section> 


			<!--   Le Footer ou pied de page du SAV  -->
			<footer class="footercontact">
				<div class="dive1">
					<div class="logof">
						<img src="Images/mth-logo.png">
						<p class="Mediate">MediaTec SARL</p>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium amet accusamus iure illum quidem saepe nam veritatis porro, ducimus aperiam delectus. <br><br> Architecto quos similique sit libero officia molestias accusantium ipsa a facilis sunt ab maxime dolorum modi labore? 
						<br><br>
						<b class="CopyRight"style=" font-size: 12px; color: #666">CopyRight 2019 MediaTeC || Solution</b>
					</p>
				</div>


				<div class="dive2">
					<p class="service"><a href="services.html">NOS SERVICES</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					<p style="position: relative; top: -100px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo nihil assumenda architecto, officia totam facere.</p>
				</div>


				<div class="dive3">
					<p class="service"><a href="contacter.html">CONTACTS</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					
				</div>



				<div class="dive4">
					<p class="service"><a href="#">PARTENAIRES</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					<div class="partner">
						<div><a href=""><img style="height: 50px; width: 50px; top: 0px;" src="Images/HP_logo_2012.png" alt=""></a></div>
						<div><a href=""><img style="height: 45px; width: 55px; top: 0px;" src="Images/Dell_Logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 15px;" src="Images/Canon_logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/Lexmark-Logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/microsoft_PNG16.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/Cisco_logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 60px;" src="Images/D-Link.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 60px;" src="Images/Lenovo-logo-2015.png" alt=""></a></div>
					</div>
				</div>	
			</footer>


		</body>
	</html>
	<script type="text/javascript" src="Styles/CodeJS.js"></script>
	<script type="text/javascript">
		
	</script>