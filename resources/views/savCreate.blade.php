@extends("layout.layout")

@section('content')
    <!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voir un Sav</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

</body>
</html>

<div class="row card p-3 m-2">

    @if($errors->any())


    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        @foreach($errors->all() as $error)


        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <li>
            {!!$error!!}
        </li>
        @endforeach
    </div>
    @endif
    <form method="POST" action="sav.store" >
        @csrf
        <div class="row">
        <div class="col-md-4">
            <label class="control-label">
                Nom Clients

            </label>
            <input name="nom_client"   type="text" class="form-control">

        </div>

        <div class="col-md-4">
            <label class="control-label" for="" >
             Email Clients

            </label>
            <input name="email_client" type="email" class="form-control">
        </div>
        <div class="col-md-4">
            <label class="control-label" for="" >
                Contact Client

            </label>
            <input name="contact_client" type="number" class="form-control">
        </div>
            <div class="col-md-4">
                <label class="control-label" for="" >
                    Nom Technicien

                </label>
                <input name="nom_tech" type="text" class="form-control">
            </div>
            <div class="col-md-4">
                <label class="control-label" for="" >
                Email Technicien
                </label>
                <input name="email_tech" type="text" class="form-control">
            </div>
            <div class="col-md-4">
                <label class="control-label" for="" >
                    Contact Technicien

                </label>
                <input name="contact_tech" type="text" class="form-control">
            </div>
            <div class="col-md-6 offset-3">
                <label class="control-label" for="" >
                    Date Entree

                </label>
                <input name="DateSav" type="date" class="form-control">
            </div>
            <div class="col-md-6 offset-3">
                <label class="control-label" for="" >
                   Quantite

                </label>
                <input  name="qte" type="number" class="form-control ">
            </div>
            <div class="col-md-6 offset-3">
                <label class="control-label" for="" >
                   Numero de Serie

                </label>
                <input name="serie" type="number" class="form-control">
            </div>
            <div class="col-md-6 offset-3">
                <label class="control-label" for="" >
                   Options

                </label>
                <select  name="option" class="form-control select2-selection__rendered">
                    <option  value="maintenance">Maintenance</option>
                    <option selected value="garantie">Equipement sous Garantie</option>
                </select>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="" >
                    Designation

                </label>
                <textarea rows="7" name="designation" class="form-control"></textarea>
            </div>

            <div class="col-md-6">
                <label class="control-label" for="" >
                   Motif

                </label>
                <textarea rows="7" name="motif" class="form-control"></textarea>
            </div>

        </div>
        <input type="submit"class="btn btn-primary btn-block p-2 m-2">
    </form>
</div>
</div>

@stop
