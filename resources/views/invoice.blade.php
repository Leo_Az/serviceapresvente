<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Facture SAV</title>
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    <style type="text/css">

        header{
            padding: 25px;
        }
        .logo img{
            height: 70px;
            width: 240px;
            margin-left: 60px;
        }
        ul{
            padding-top: 10px;
            font-weight: bold;
        }
        .date p{
            line-height: 2em;
        }
        .bon{
            padding: 0px 0px;
        }
        .corps table{
            margin: 0px 65px;
            width: 90%;
        }
        .designe td,th{
            border: solid 1px #999;
            text-align: center;
        }
        .corps th{
            height: 40px;
        }
        .designe td{
            height: 270px;
        }
        .frais td{
            height: 40px;
        }
        .rien td{
            border: none;
        }

        .info-entre{
            margin: 0px 130px;
            margin-top: 70px;
            width: 80%;
            border: solid 1px #999;
        }
        .info-entre th{
            height: 40px;
        }
        .infos{
            width: 80%;
            border: solid 1px #999;
            margin: 0px 130px;
            margin-bottom: 50px;
        }
        .infos td{
            height: 50px;
            padding: 10px 50px;
        }
        .info-client{
            width: 100%;
            margin: 0px 70px;
            border: solid 1px #999;
        }
        .client td{
            height: 50px;
            padding: 10px 50px;
        }

        .info-client th{
            height: 40px;
        }

        .client{
            width: 100%;
            border: solid 1px #999;
            margin: 0px 70px;
            margin-bottom: 50px;
        }

        .info-techn{
            width: 100%;
            margin: 0px 150px;
            border: solid 1px #999;
        }
        .techn td{
            height: 50px;
            padding: 10px 50px;
        }

        .info-techn th{
            height: 40px;
        }

        .techn{
            width: 100%;
            border: solid 1px #999;
            margin: 0px 150px;
            margin-bottom: 10px;
        }
        h2{
            font-size: 14px;
            font-weight: bold;
            margin: 0px 85px;
            margin-top: -40px;
        }

        .footer p{
            text-align: center;
            font-weight: 600;
        }
        .avertir p{
            font-weight: 600;
            line-height: 0.9em;
        }
        .avertir{
            margin: 50px 70px;
        }
        hr{
            width: 95%;
            height: 5px;
            color: #999;
        }


    </style>
</head>
<body>
<header class="container-fluid">
    <div class="row">
        <div class="col-md-5 logo">
            <img src="images/mediatec.PNG">
            <ul>
                <li>SERVICES INFORMATIQUES</li>
                <li>BUREAUTIQUE</li>
                <li>TRAVAUX DIVERS</li>
            </ul>
        </div>
        <div class="col-md-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="sav" id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">
                    <b>Equipement sous Garantie (SAV)</b>
                </label>
            </div>
            <br>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="sav" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">
                    <b>Maintenance</b>
                </label>
            </div>

        </div>

        <div class="col-md-3 date">
            <p>Date d'entrée : <b>20 Avril 2019</b></p>
            <p>Date de sortie : <b>19 Mai 2019</b></p>
        </div>
    </div>

    <div class="row bon">
        <div class="col-md-5">
        </div>

        <div class="col-md-7">
            <h1>BON DE RECEPTION N° : <b style="color: red">MDTC/0001</b></h1>
        </div>
    </div>
</header>

<div class="container-fluid corps">
    <table>
        <tr class="designe">
            <th width="10%">Quantité</th>
            <th width="45%">Désignation</th>
            <th width="25%">Motif</th>
            <th width="20%">N° de Série</th>
        </tr>
        <tr class="designe">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="frais">
            <td></td>
            <td></td>
            <th>FRAIS DE DIAGNOSTIQUE</th>
            <th></th>
        </tr>
    </table>
</div>

<div class="container-fluid avertir">
    <p>Après délai de récupération (7 Jours ouvrables à compter de l'appel du Service Technique) non respecté par le client l équipement fera</p>
    <p>l'objet de frais de magasinage et MEDIATEC se désengage de toute responsabilité des éventuels problèmes survenus après cette date.</p>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <table class="info-client">
                <tr>
                    <th>INFORMATIONS DU CLIENT</th>
                </tr>
            </table>
            <table class="client">
                <tr>
                    <td width="55%" style="padding-top: 40px; padding-left: 70px;">Raison Sociale : <b>Gnakalé</b></td>
                </tr>
                <tr>
                    <td width="55%" style="padding-left: 70px;">Contact : <b>67 50 96 22</b></td>
                </tr>
                <tr>
                    <td width="55%" style="padding-bottom: 40px; padding-left: 70px;">E-mail : <b>roland@gmail.com</b></td>
                </tr>
            </table>
        </div>


        <div class="col-md-5">
            <table class="info-techn">
                <tr>
                    <th>INFORMATIONS DU TECHNICIEN</th>
                </tr>
            </table>
            <table class="techn">
                <tr>
                    <td width="55%" style="padding-top: 10px; padding-left: 70px;">Nom : <b>Bati</b></td>
                </tr>
                <tr>
                    <td width="55%" style=" padding-left: 70px;">Prénom : <b>Christian</b></td>
                </tr>
                <tr>
                    <td width="55%" style=" padding-left: 70px;">Contact : <b>78 45 12 36</b></td>
                </tr>
                <tr>
                    <td width="55%" style="padding-bottom: 10px; padding-left: 70px;">E-mail : <b>bati@gmail.com</b></td>
                </tr>
            </table>
        </div>
        <h2>NOUS VOUS REVENONS DANS 72 HEURES MAXIMUM</h2>

    </div>
</div>
<hr>
<div class="container-fluid footer">
    <p>Abidjan - Plateau Av. Franchetd'Esperey Imm. Abeille - 17 BP 1316 Abidjan 17 Tel: <b>2021844</b> Fax: <b>67509622</b></p>
    <p>C.C.N° 1224195 P -R.C.N° CI-ABJ-2011-B2020 - Compte Bancaire 00269240004 BNI / <b>www.mediatec-ci.com</b> / E-mail: <b>infos@mediatec-ci.com</b></p>
</div>
</body>
</html>