<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email',
            'password'=>'required|min:6',
        ];
    }
    public function message()
    {
        return [
            'email.required'=>'Il me faut une ardresse email',
            'email.email'=>'Ceci n est pas un Adresse mail valide !',
            'password.required'=>'il me faut un mot de passe',
            'password.min'=>'Mot de passe doit fait au moins 6 caracteres',
        ];
    }
}
