<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\RegistersRequest;
use App\Mail\RegisterMail;
use App\Notifications\SendUserMailAfterRegister;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Mail;

class InscriptionController extends Controller
{
    public function __construct()
    {
       if ($this->middleware('guest')->only('create','store'));
       else
           return redirect('/inscription/create');
    }
    public function create(){

    return view('inscription');
}

    public function store (RegistersRequest $request)
    {

        $request['token'] = str_random(60);

        $user = User::create($request->all());

//        $user= User::create([
//            'name'=>$request->name,
//            'email'=>$request->email,
//            'sexe' => $request->sexe,
//            'password' =>\Hash::make( $request->password),
//            'phone'=> $request->phone,
//            'prenom'=> $request->prenom,





//     Mail::to($user)->send(new RegisterMail($user));



      $user->notify(new SendUserMailAfterRegister());


        return redirect('MessageMail');
    }
}
