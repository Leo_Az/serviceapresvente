<?php

namespace App\Http\Controllers\Auth;
use App\Http\Requests\Auth\LoginsRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginsController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['destroy']);
    }

    public function create(){

        return view('auth.login');

    }

    public function store(LoginsRequest $request)
    {
        $user = User::where('name', $request->name)
            ->orWhere('email', $request->email)
            ->first();

        if ($user) {
            if (\Hash::check($request->password, $user->password)) {
                auth()->login($user,$request->has('remember'));

                if (auth()->user()->isAdmin())
                    return redirect('/admin');

                return redirect('/reserve');
            }
            return $this->messageError($request);
        }
        return $this->messageError($request);
    }


    /**
     * Message d'erreur
     * @param $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function messageError($request)
    {
        return back()->with([
            'color' => 'red',
            'message' => "Identifiant incorrect!"
        ])->withInput([
            'name' => $request->name,
        ]);
    }

    public function destroy(){


        auth()->logout();


        return redirect('/');
    }

    public function update(User $user, Request $request)
    {
        $user->update(['is_admin' => true]);

        return $user;
    }

}
