<?php

namespace App\Http\Controllers;

use App\reserve;
use App\Sav;
use function Faker\Provider\pt_BR\check_digit;
use Illuminate\Http\Request;

class ReserveController extends Controller
{




    public function show(){


            if (auth()->guest()){

                return abort(403);
            }
      $user=auth()->user();

        return view('reserve', [

            'user'=>$user

        ]);





    }



    public function store(){

        if(auth()->guest()){

            abort(403);
        }
        request()->validate([

            'name'=>'required|min:6',
            'designationctl'=>'required',
            'seriectl'=>'required',
            'motifctl'=>'required',
            'contactctl'=>'required',
            'emailctl'=>'required|email',
            'dateAchat'=>'required|date',




        ]);

        reserve::create([

            'name'=>\request('name'),
            'designationctl'=>\request('designationctl'),
            'seriectl'=>\request('seriectl'),
            'motifctl'=>\request('motifctl'),
            'contactctl'=>\request('contactctl'),
            'emailctl'=>\request('emailctl'),
            'dateAchat'=>\request('dateAchat'),

        ]);

        return redirect('MessageSoumie');
    }

}
