<?php

namespace App\Http\Controllers;

use App\Gate;
use Illuminate\Http\Request;

class MachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gate = Gate::all();

        return view('garte',[

            'gate'=> $gate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->guest()){

            abort(403);
        }
        return view('gate-create');
    }

    /**
     * Store a newly created resource in storage.
    * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->guest()){

            abort(403);
        }
        Request()->validate([

            'Designation'=>'required|min:8',
            'numero'=>'required',
            'date'=>'required|date',
            'panne'=>'required',
            'NomClient'=>'required|min:8',
            'ContactClient'=>'required',


        ]);


        Reparer::create([
            'Designation'=>\request('Designation'),
            'numserie'=>\request('numserie'),
            'date'=>\request('date'),
            'Motif'=>\request('Motif'),
            'Methode'=>\request('Methode'),
            'NomClient'=>\request('Nomclient'),
            'ContactClient'=>\request('ContactClient'),

        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
