<?php

namespace App\Http\Controllers;
use App\Sav;
use Illuminate\Http\Request;

class SavController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function index()
    {

        if (auth()->guest()){

            return abort(403);
        }
        $savs = Sav::all();
        return view('layout/sav',[

            'savs'=>$savs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (auth()->guest()){

            return abort(403);
        }
        return view('savCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        if (auth()->guest()){

            return abort(403);
        }
        request()->validate([
            'nom_client'=>'required',
            'email_client'=>'required|email',
            'contact_client'=>'required',
            'contact_client'=>'required',
            'nom_tech'=>'required',
            'email_tech'=>'required|email',
            'contact_tech'=>'required',
            'DateSav'=>'required|date',
            'qte'=>'required',
            'serie'=> 'required',
            'option'=>'required',
            'designation'=>'required',
            'motif'=>'required',




        ]);
        sav::create([

            'nom_client'=> \request('nom_client'),
            'email_client'=> \request('email_client'),
            'contact_client'=> \request('contact_client'),
            'nom_tech'=> \request('nom_tech'),
            'email_tech'=>\request('email_tech'),
            'contact_tech'=>\request('contact_tech'),
            'DateSav'=> \request('DateSav'),
            'qte'=> \request('qte'),
            'serie'=> \request('serie'),
            'option'=>\request('option'),
            'designation'=>\request('designation'),
            'motif'=>\request('motif'),



        ]);

        return redirect ('sav.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (auth()->guest()){

            return abort(403);
        }
        $sav = Sav::where('id',$id)->first();

        return view('savShow',[

            'sav'=> $sav
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
