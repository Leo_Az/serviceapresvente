<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class AdminController extends Controller
{


    public function show(){


        if(auth()->guest()){

            abort(403);
        }

        return view('admin-sav');
    }
}
