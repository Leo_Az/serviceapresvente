<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use FontLib\Table\Type\name;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Admin extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset(User::all()->name)){

            return view('/');
        }

    }
}
