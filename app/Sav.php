<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sav extends Model
{
    protected $fillable = [
        'nom_client', 'email_client','contact_client','nom_tech','email_tech','contact_tech','DateSav','qte','motif','designation','serie','option'
    ];
}
