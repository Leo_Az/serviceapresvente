<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','IndexController@index');




Route::get('/contact','ContactController@contact');




Route::get('/service','ServiceController@service');




Route::get('/inscription/create','InscriptionController@create')->name('inscription');




Route::post('/inscription','InscriptionController@store')->name('inscriptions');




Route::get('/confirmation/{user}/{token}/','ConfirmationController@store')->name('confirmation.store');



//Route::get('/confirmation/{user}/{token}','ConfirmationController@store')->name('confirmation.store');




Route::get('sav.create', 'SavController@create')->name('sav.create');




Route::post('sav.store', 'SavController@store')->name('sav.store');




Route::get('sav{sav}','SavController@index')->name('sav.index');



Route::get('sav/{sav}','SavController@show')->name('sav.show');




Route::get('sav/{sav}/edit','SavController@edit');




Route::patch('sav/{sav}','SavController@update');




Route::delete('sav/{sav}','SavController@destroy');



//Route::get('/admin','AdminController@show')->name('admin');




Route::get('monservice/list','listControler@list');



Route::get('/savCreate/',' savController@create')->name('/savCreate');



Route::group(['namespace'=>'Auth'],function (){


    Route::get('login','LoginsController@create')->name('login');
    Route::post('login','LoginsController@store');

    Route::get('logout','LoginsController@destroy')->name('logout');



});

// Deconnexion des utilisateur


Route::get('/attente/','AttenteController@index');
Route::get('/attente/{res}','AttenteController@show')->name('attente.show');




// passse un sav

Route::get('reserve','ReserveController@show')->name('reserve.sav');




Route::post('reserve','ReserveController@store');




Route::get('/admin/', 'HomeController@index')->name('admin');


//
//
//Route::group(['namespace'=>'vente'],function (){
//
//
//    Route::post('vente','VenteController@store')->name('vente.store');
//
//});




Route::get('vente','VenteController@index');
Route::get('vente/{qs}/','VenteController@show')->name('Vente.show');


Route::get('MessageSoumie','MessageSoumieController@index')->name('MessageSoumie');

Route::get('MessageMail','MessageSoumieController@mail')->name('MessageMail');



//Auth::routes();



Route::get('personne','PersonneController@index');



Route::get('personne/{personne}/','PersonneController@show')->name('personne-view');


//génération de facture


Route::get('facture/','FactureController@index');




Route::get('/facture/{id}','FactureController@facture');


//contactter nous


Route::post('/contacter','ContactController@store');

Route::get('messagerie','ContactController@index');




Route::get('messagerie/{contacts}/','ContactController@show')->name('messagerie.show');

//option messagerie

Route::get('reparer','ReparationController@index');



Route::get('reparer.create','ReparationController@create')->name('reparer.create');


Route::post('reparer.store','ReparationController@store')->name('reparer.store');


Route::get('reparer.show/{reparer}','ReparationController@show')->name('reparer.show');
//edition de profile



Route::put('user');
//

Route::get('garte','MachineController@index');

Route::get('gate.create','MachineController@create')->name('gate.create');


Route::post('gate.store','MachineController@store')->name('gate.store');



Route::get('contacter',function(){

    return view('contacter');
});




Route::get('services',function(){

    return view('services');
});

